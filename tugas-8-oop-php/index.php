<?php
require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

$sheep = new Animal("shaun");
echo "Name: " .$sheep->name ."<br>"; // "shaun"
echo "Legs: " .$sheep->legs ."<br>"; // 4
echo "Cold blooded: " .$sheep->cold_blooded. "<br><br>"; // "no"

$sungokong = new Ape("kera sakti");
echo "Nama hewan: " .$sungokong->name ."<br>"; // "kera sakti"
echo "Legs: " .$sungokong->legs ."<br>"; // 4
echo "Cold blooded: " .$sungokong->cold_blooded. "<br>"; // "no"
echo "Yell: " .$sungokong->yell(). "<br><br>"; // "Auooo"

$kodok = new Frog("buduk");
echo "Nama hewan: " .$kodok->name ."<br>"; // "buduk"
echo "Legs: " .$kodok->legs ."<br>"; // 4
echo "Cold blooded: " .$kodok->cold_blooded ."<br>"; // "no"
echo "Jump: " .$kodok->jump() ; // "hop hop"
?>