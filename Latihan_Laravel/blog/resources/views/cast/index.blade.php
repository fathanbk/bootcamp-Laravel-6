@extends('layouts.master')

@section('title')
    Index
@endsection

@section('content')
    
<a href="/cast/create" class="btn btn-primary">Tambah</a>
<table class="table table-striped">
    <thead >
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key=>$item)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$item->nama}}</td>
                <td>{{$item->umur}}</td>
                <td>{{$item->bio}}</td>
                <td class="form-inline justify-content-between">
                    <a href="/cast/{{$item->id}}" class="btn btn-info">Detail</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-primary">Edit</a>
                    <form action="/cast/{{$item->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger my-1" item="Delete" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>
@endsection