@extends('layouts.master')
@section('title')
    Edit Cast
@endsection
@section('content')
<h2>Editing {{$cast->nama}}</h2>
<div class="form-group">
    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('PUT')
    <label for="title">Nama</label>
    <input type="text" value="{{$cast->nama}}" class="form-control" name="nama" id="nama" placeholder="Masukkan nama">
    @error('nama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>
<div class="form-group">
    <label for="body">Umur</label>
    <input type="text" value="{{$cast->umur}}" class="form-control" name="umur" id="umur" placeholder="Masukkan umur">
    @error('umur')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>
<div class="form-group">
    <label for="bio">Bio</label>
    <textarea name="bio" " class="form-control" cols = 30 rows = 10> {{$cast->bio}} </textarea>
    @error('bio')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
    @enderror
</div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
</div>
@endsection