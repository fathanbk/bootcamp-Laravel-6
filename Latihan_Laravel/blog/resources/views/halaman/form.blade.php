
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Sign Up SanberBook</title>
</head>

<body>
  <h1>Buat Account Baru!</h1>
  <h2>Sign Up Form</h2>
  <form action="/send" method="post">
    @csrf
  <label for="firstName">First Name: </label>
  <br /><br />
  <input type="text" id="firstName" name="first_name" style="text-transform: capitalize" />
  <br /><br />

  <label for="lastName">Last Name: </label>
  <br /><br />
  <input type="text" id="lastName" name="last_name" style="text-transform: capitalize"/>
  <br /><br />

  <label>Gender: </label>
  <br /><br />
  <input type="radio" id="gender1" name="gender" value="Male" />
  <label for="gender1"> Male</label>
  <br />
  <input type="radio" id="gender2" name="gender" value="Female" />
  <label for="gender2"> Female</label>
  <br />
  <input type="radio" id="gender3" name="gender" value="Other" />
  <label for="gender3"> Other</label>
  <br /><br />

  <label>Nationality: </label>
  <br /><br />
  <select name="nationality">
    <option value="Indonesian">Indonesian</option>
    <option value="Singaporean">Singaporean</option>
    <option value="Malaysian">Malaysian</option>
    <option value="Australian">Australian</option>
  </select>
  <br /><br />

  <label>Languange Spoken: </label>
  <br /><br />
  <input type="checkbox" name="language" value="Bahasa Indonesia" />
  <label for="language1"> Bahasa Indonesia</label>
  <br />
  <input type="checkbox" id="language2" name="language" value="English" />
  <label for="language2"> English</label>
  <br />
  <input type="checkbox" id="language3" name="language" value="Other" />
  <label for="language3"> Other</label>
  <br /><br />

  <label for="bio">Bio: </label>
  <br /><br />
  <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
  <br />

  <button>Sign Up</button>
</form>
</body>

</html>