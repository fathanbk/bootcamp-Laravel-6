<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
public function formulir()
{
    return view('halaman/form');
}

public function submit(Request $request)
{
//    dd($request->all());
    $namaDepan = $request["first_name"];
    $namaBelakang = $request["last_name"];
    return view('halaman.welcome',compact('namaDepan','namaBelakang'));
}
}
